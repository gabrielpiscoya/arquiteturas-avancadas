# DON'T FORGET TO: source /opt/intel/bin/compilervars.sh intel64
all: pi.exec 
	@echo "Compiled everything! Yes!"

pi.exec: pi.c
	gcc -o pi.exec pi.c -O2 -fopenmp -faggressive-loop-optimizations -fira-loop-pressure

clean:
	rm -f *.exec *.optrpt *.exec.stackdump
	
make love:
	make clean
	make
