#!/bin/bash

mkdir results

for THREADS in 1 2 4 8 16 24
do
        mkdir ./results/$THREADS

        echo "Starting NUMTHREADS: $THREADS"
        i=1
        while [ $i -le 10 ]
        do
            echo "$i"
            for ITERATIONS in 1 10 100 1000 10000 100000 1000000 10000000 100000000 1000000000
            do
                #echo "Time elapsed;Pi Estimative" > ./results/$THREADS/$ITERATIONS.csv

                        ./pi.exec $ITERATIONS $THREADS >> ./results/$THREADS/$ITERATIONS.csv
                        #sleep 1
            done
            i=$((i+1))
        done
        
        cd ./results/$THREADS
        for f in *.csv
        do
            sed -i -e 's/\./,/g' "${f}"
            (cat "${f}"; echo) >> tudo$THREADS.csv
        done
        
        cd ../../
done


