/***********************************************************************/ 
//
//					INF-01191 - Arquiteturas Avançadas 
//								Trabalho 1
//			Calculo de Pi utilizando método Montecarlo e OpenMP
//				Gabriel Piscoya Dávila 		00246031
//				Rubens Ideron dos Santos	00243658	
/**********************************************************************/
/* Program to compute Pi using Monte Carlo methods */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
//#include <time.h>
#include <assert.h>
#define PPI 2

// Compute a pseudorandom double 
// from a random integer between 0 and 32767
// Output value in range [0, 1]

double pi_montecarlo_parallel(int niter)
{
   double x[2] ;
   double y[2] ;
   //double w[2] ;
   //double z[2] ;

   double rx,ry;
   double rw,rz;
	
   unsigned int i, count; /* # of points in the 1st quadrant of unit circle */
   double pi;
   unsigned int g_seed;
   int contador;
   int j;
   count = 0;
#pragma omp parallel private(g_seed, x, y)
{
   	/* initialize seeds - one for each thread */
   	g_seed = time(NULL)*(omp_get_thread_num()+1);

#pragma omp for schedule(static) reduction(+:count)
   for ( i=0; i<niter; i++) {
      g_seed = (214013*g_seed+2531011);
      x[0] = (double)((g_seed>>16)&0x7FFF)/32767;
      g_seed = (214013*g_seed+2531011);
      y[0] = (double)((g_seed>>16)&0x7FFF)/32767;

   /*   g_seed = (214013*g_seed+2531011);
      w[0] = (double)((g_seed>>16)&0x7FFF)/32767;
      g_seed = (214013*g_seed+2531011);
      z[0] = (double)((g_seed>>16)&0x7FFF)/32767;
	*/
// testar calculando mais pontos ao mesmo tempo

/*	#pragma simd
	for(j=0;j<1;j++){
		x[j+1] = x[j];
		y[j+1] = y[j];	

		w[j+1] = w[j];
		z[j+1] = z[j];
	}
*/
		x[1] = x[0];
		y[1] = y[0];
		rx = 1.0;
		ry = 1.0;	
		//rw = 1.0;
		//rz = 1.0;
		#pragma simd
		for(contador=0;contador<PPI;contador++){
			rx = rx * x[contador];
			ry = ry * y[contador];
			//rw = rw * w[contador];
			//rz = rz * z[contador];

		}
		//printf("x=%f,y=%f, thread=%d\n",x,y, omp_get_thread_num());
      if (rx+ry<=1) count++;
      //if (rw+rz<=1) count++;

      }
}
	printf("count:%d\n",count);
   pi=(double)count/niter*4;
	return pi;
}

double pi_montecarlo_sequential(int niter)
{
	double x,y;
    unsigned int i,count=0; /* # of points in the 1st quadrant of unit circle */
    double pi;
	
   srand(time(NULL));

   for ( i=0; i<niter; i++) {
		x =	(double)rand()/RAND_MAX;
		y =  (double)rand()/RAND_MAX;
	      if (x*x+y*y<=1) count++;
       }

	//printf("Count:%d\n",count);
    pi=(double)count/niter*4;
	return pi;
}

int main(int argc, char** argv)
{

    struct timeval time_start, time_end;
    double elapsed;
	int n;
    int nthreads;
	double pi;
   
    assert(argc == 3);
   
	n = atoi(argv[1]);
    nthreads = atoi(argv[2]);

	omp_set_num_threads(nthreads); 

	gettimeofday(&time_start, NULL);
	pi = pi_montecarlo_parallel(n);
	gettimeofday(&time_end, NULL);

	elapsed = time_end.tv_sec - time_start.tv_sec + (time_end.tv_usec - time_start.tv_usec) / 1000000.0;

    printf("# of trials= %d , estimate of pi is %g \n",n,pi);
	printf("time: %.6f seconds\n",elapsed);
	
	return 0;
}
